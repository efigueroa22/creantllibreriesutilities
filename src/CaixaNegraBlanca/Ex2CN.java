package CaixaNegraBlanca;

import java.util.Arrays;
import java.util.Scanner;

public class Ex2CN {
	
	public static double adivinanumeros(int num1, int num2, int num3, int num4, int num5) {
		Scanner sc = new Scanner(System.in);
		
		int[] numsFixos = {2, 8, 16, 25, 33, 42, 52, 63, 75, 88};
		
        int[] numsUsuari = new int[5];
        
        numsUsuari[0] = num1;
        numsUsuari[1] = num2;
        numsUsuari[2] = num3;
        numsUsuari[3] = num4;
        numsUsuari[4] = num5;
        
        int encerts = 0;
        
        for (int num : numsUsuari) {
            if (Arrays.binarySearch(numsFixos, num) >= 0) {
                encerts++;
            }
        }
        
        double percentEncerts = encerts / 5.0 * 100;
        
        System.out.println("Has encertat " + encerts + " numeros.");
        System.out.println("El percentatge d'encerts es: " + percentEncerts + "%");
        sc.close();
		return percentEncerts;
	}
}