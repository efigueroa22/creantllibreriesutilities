package CaixaNegraBlanca;

public class Ex1CN {

	public static double conversorCelsiusFahrenheit(double celsius) {
        double fahrenheit = celsius * 1.8 + 32;
        System.out.println(celsius + " --> " + fahrenheit);
        return fahrenheit;
	}
}