package creant_llibreries_utilities;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import java.io.File;

public class UtilConsole {

	static Scanner sc = new Scanner(System.in);
	
	public static int demanarInt(String etiqueta) {
		int entero = 0;
		boolean isEntero = false;
		
		do {
			System.out.println(etiqueta);
			if(sc.hasNextInt()) {
				entero = Integer.parseInt(sc.nextLine());
				isEntero = true;
			}
			else {
				// para saltar el valor introducido (que no es entero)
				sc.nextLine();
			}
		}while(!isEntero);
		
		return entero;
	}
	
	public static double demanarDouble(String etiqueta) {
		double decimal = 0;
		boolean isDouble = false;
		
		do {
			System.out.println(etiqueta);
			if(sc.hasNextDouble()) {
				decimal = sc.nextDouble();
				sc.nextLine(); //limpiar en \n
				isDouble = true;
			}
			else {
				// para saltar el valor introducido (que no es entero)
				sc.nextLine();
			}
		}while(!isDouble);
		
		return decimal;
	}
	
	public static String demanarString(String etiqueta) {
		String cadena = "";
		boolean isString = false;
		
		do {
			System.out.println(etiqueta);
			if(sc.hasNext()) {
				cadena = sc.nextLine();
				isString = true;
			}
			else {
				sc.nextLine();
			}
		}while(!isString);
		
		return cadena;
	}
	
	public static String demanarDNI(String etiqueta) {
		String dni;
		boolean dniOK = false;
		do {
			dni = demanarString(etiqueta);
			dniOK = validarDNI(dni);
		}while(!dniOK);
		
		return dni;
	}
	
	public static boolean validarDNI(String dni) {
		String lletres = "TRWAGMYFPDXBNJZSQVHLCKET";
		
		if(dni.matches("[0-9]{7,8}[A-Z a-z]")){
			int numDNI = Integer.parseInt(dni.substring(0,dni.length()-1));
			char lletraDNI = dni.charAt(dni.length()-1);
			int mod = numDNI % 23;
			if(lletres.charAt(mod) == lletraDNI) return true;
		}
		
		return false;
	}
	
	public static String demanarCP(String etiqueta) {
		String cp;
		do {
			cp = demanarString(etiqueta);
		}while(!cp.matches("[0-9]{5}"));
		
		return cp;
	}
	
	public static String demanarEmail(String etiqueta) {
		//https://howtodoinjava.com/java.regex/java-regex-validate-email-address/
		String email;
		do {
			email = demanarString(etiqueta);
		}while (!email.matches("^(.+)@(.+)$"));
		
		return email;
	}
	
	public static String demanarTelefon(String etiqueta) {
		String telefon;
		do{
			telefon = demanarString(etiqueta);
		}while(!telefon.matches("[6-7][0-9]{8}"));
		
		return telefon;
	}
	
	public static LocalDate demanarData(String etiqueta) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		// DateTimeFormatter dtf = DteTimeFormatter.ofLocaliezDate(FormatStyle.SHORT);
		String dataString = "";
		LocalDate data = null;
		boolean isDate = false;
		
		do {
			dataString = demanarString(etiqueta);
			try {
				data = LocalDate.parse(dataString, dtf);
				isDate = true;
			} catch (DateTimeParseException e) {
				System.out.println("Fecha incorrecta, formato correcto dd/mm/aaaa");
			}
		}while(!isDate);
		
		return data;
	}
	
	public static String demanarTelefonFix(String etiqueta) {
		String telefon;
		do{
			telefon = demanarString(etiqueta);
		}while(!telefon.matches("[9-3][0-9]{8}"));
		
		return telefon;
	}
	
	public static boolean demanarboolean(String etiqueta) {
        String bool;
        do {
            bool = demanarString(etiqueta);
        } while (!bool.matches("[0-1]{1}"));
        if (bool.equals("0")) {
            return false;
        }else {
            return true;
        }
    }
	
	public static String demanarRuta(String etiqueta) {		
		File fitxer;
		do {
			fitxer = new File(etiqueta);
		}while(!fitxer.isFile() || !fitxer.exists());
		return etiqueta;
    }
	
	public static boolean confirmar(String etiqueta) {
        return "confirmar".equals(etiqueta) || "Confirmar".equals(etiqueta);
    }
}