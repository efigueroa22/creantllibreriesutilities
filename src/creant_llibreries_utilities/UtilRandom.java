package creant_llibreries_utilities;
import java.util.Random;

public class UtilRandom {

    private static final Random random = new Random();

    public static int generarNumero() {
        return random.nextInt();
    }

    public static String generarColor() {
        String[] colors = { "negre", "blanc", "vermell", "blau", "verd", "groc", "marro", "tronja", "lila", "rosa"};
        return colors[random.nextInt(colors.length)];
    }

    public static boolean generarBoolean(double probabilitat) {
        return random.nextDouble() < probabilitat;
    }

    public static String generarTelefon() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            sb.append(random.nextInt(9));
        }
        return sb.toString();
    }

    public static String generarData() {
        int any = random.nextInt(100) + 1920;
        int mes = random.nextInt(12) + 1;
        int dia;
        switch (mes) {
            case 2:
                dia = random.nextInt(28) + 1;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dia = random.nextInt(30) + 1;
                break;
            default:
                dia = random.nextInt(31) + 1;
                break;
        }
        return String.format("%02d/%02d/%04d", dia, mes, any);
    }

    public static String generarUsuari() {
        String[] adjectius = { "content", "viatjer", "valent", "pispat", "cridaner", "inteligent" };
        String[] noms = { "pirata", "elefant", "conductor", "tigre", "gos", "gat" };
        String[] numeros = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10" };
        return adjectius[random.nextInt(adjectius.length)] + noms[random.nextInt(noms.length)] +
                numeros[random.nextInt(numeros.length)];
    }
}