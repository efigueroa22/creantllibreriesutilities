package creant_llibreries_utilities;

import java.util.function.Predicate;

public enum DiaSetmana {
    DILLUNS("Dilluns"),
    DIMARTS("Dimarts"),
    DIMECRES("Dimecres"),
    DIJOUS("Dijous"),
    DIVENDRES("Divendres"),
    DISSABTE("Dissabte"),
    DIUMENGE("Diumenge");

    private final String nom;
    private Predicate<DiaSetmana> esDiumenge;

    DiaSetmana(String nom) {
        this.nom = nom;
        this.esDiumenge = d -> false;
    }

    private void init(Predicate<DiaSetmana> esDiumenge) {
        this.esDiumenge = esDiumenge;
    }

    DiaSetmana(String nom, Predicate<DiaSetmana> esDiumenge) {
        this.nom = nom;
        init(esDiumenge);
    }

    public boolean esDiumenge() {
        return esDiumenge.test(this);
    }

    @Override
    public String toString() {
        return nom;
    }

    static {
        DIUMENGE.init(d -> d == DIUMENGE);
    }
}